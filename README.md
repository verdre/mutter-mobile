# Mutter Mobile

See https://gitlab.gnome.org/verdre/gnome-shell-mobile/.

## License

Mutter is distributed under the terms of the GNU General Public License,
version 2 or later. See the [COPYING][license] file for detalis.

[license]: COPYING
