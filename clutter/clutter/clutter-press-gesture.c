/*
 * Copyright (C) 2023 Jonas Dreßler <verdre@v0yd.nl>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ClutterPressGesture:
 *
 * A #ClutterGesture subclass for recognizing press gestures
 */

#include "config.h"

#include "clutter-press-gesture.h"

#include "clutter-enum-types.h"
#include "clutter-private.h"

typedef struct _ClutterPressGesturePrivate ClutterPressGesturePrivate;

struct _ClutterPressGesturePrivate
{
  gboolean pressed;

  int cancel_threshold;

  unsigned int long_press_duration;
  unsigned int long_press_timeout_id;

  unsigned int n_presses_happened;
  unsigned int next_press_timeout_id;

  unsigned int required_button;

  gboolean is_touch;

  graphene_point_t press_coords;
  unsigned int press_button;
  ClutterModifierType modifier_state;

  gboolean is_subclassed;
};

enum
{
  PROP_0,

  PROP_CANCEL_THRESHOLD,
  PROP_LONG_PRESS_DURATION,
  PROP_PRESSED,
  PROP_REQUIRED_BUTTON,

  PROP_LAST
};

enum
{
  PRESS,
  LONG_PRESS,
  RELEASE,

  LAST_SIGNAL
};

static GParamSpec *obj_props[PROP_LAST] = { NULL, };
static unsigned int obj_signals[LAST_SIGNAL] = { 0, };

G_DEFINE_TYPE_WITH_PRIVATE (ClutterPressGesture, clutter_press_gesture, CLUTTER_TYPE_GESTURE)

static unsigned int
get_default_long_press_duration (void)
{
  ClutterSettings *settings = clutter_settings_get_default ();
  int long_press_duration;

  g_object_get (settings,
                "long-press-duration", &long_press_duration,
                NULL);

  return long_press_duration;
}

static unsigned int
get_next_press_timeout_ms (void)
{
  ClutterSettings *settings = clutter_settings_get_default ();
  int double_click_time;

  g_object_get (settings,
                "double-click-time", &double_click_time,
                NULL);

  return double_click_time;
}

static unsigned int
get_default_cancel_threshold (void)
{
  return 36;
}

static gboolean
long_press_cb (gpointer user_data)
{
  ClutterPressGesture *self = user_data;
  ClutterPressGesturePrivate *priv =
    clutter_press_gesture_get_instance_private (self);

  if (priv->is_subclassed)
    {
      if (CLUTTER_PRESS_GESTURE_GET_CLASS (self)->long_press)
        CLUTTER_PRESS_GESTURE_GET_CLASS (self)->long_press (self);
    }
  else
    {
      if (clutter_gesture_get_state (CLUTTER_GESTURE (self)) == CLUTTER_GESTURE_STATE_RECOGNIZING)
        g_signal_emit (self, obj_signals[LONG_PRESS], 0);
    }

  priv->long_press_timeout_id = 0;
  return G_SOURCE_REMOVE;
}

static void
set_pressed (ClutterPressGesture *self,
             gboolean             pressed)
{
  ClutterPressGesturePrivate *priv =
    clutter_press_gesture_get_instance_private (self);

  if (priv->pressed == pressed)
    return;

  priv->pressed = pressed;

  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_PRESSED]);
}

static gboolean
next_press_timed_out (gpointer user_data)
{
  ClutterPressGesture *self = user_data;
  ClutterGesture *gesture = user_data;
  ClutterPressGesturePrivate *priv =
    clutter_press_gesture_get_instance_private (self);
  unsigned int active_n_points = clutter_gesture_get_n_points (gesture);

  if (active_n_points == 0)
    clutter_gesture_set_state (gesture, CLUTTER_GESTURE_STATE_CANCELLED);

  priv->next_press_timeout_id = 0;
  return G_SOURCE_REMOVE;
}

static gboolean
should_handle_sequence (ClutterGesture     *gesture,
                        const ClutterEvent *sequence_begin_event)
{
  ClutterEventType event_type = clutter_event_type (sequence_begin_event);

  if (event_type == CLUTTER_BUTTON_PRESS ||
      event_type == CLUTTER_TOUCH_BEGIN)
    return TRUE;

  return FALSE;
}

static void
point_began (ClutterGesture *gesture,
             unsigned int    sequence)
{
  ClutterPressGesture *self = CLUTTER_PRESS_GESTURE (gesture);
  ClutterPressGesturePrivate *priv =
    clutter_press_gesture_get_instance_private (self);
  unsigned int active_n_points = clutter_gesture_get_n_points (gesture);
  const ClutterEvent *event;
  gboolean is_touch;
  unsigned int press_button;
  ClutterModifierType modifier_state;
  graphene_point_t coords;
  unsigned int long_press_duration;

  if (active_n_points != 1)
    {
      clutter_gesture_set_state (gesture, CLUTTER_GESTURE_STATE_CANCELLED);
      return;
    }

  event = clutter_gesture_get_point_event (gesture, sequence);

  is_touch = clutter_event_type (event) == CLUTTER_TOUCH_BEGIN;
  press_button = is_touch ? CLUTTER_BUTTON_PRIMARY : clutter_event_get_button (event);
  modifier_state = clutter_event_get_state (event);
  clutter_gesture_get_point_coords_abs (gesture, sequence, &coords);

  if (priv->required_button != 0 &&
      press_button != priv->required_button)
    {
      clutter_gesture_set_state (gesture, CLUTTER_GESTURE_STATE_CANCELLED);
      return;
    }

  priv->n_presses_happened += 1;

  if (priv->n_presses_happened == 1)
    {
      g_assert (priv->next_press_timeout_id == 0);

      priv->is_touch = is_touch;
      priv->press_button = press_button;
      priv->modifier_state = modifier_state;
      priv->press_coords = coords;

      if (!priv->is_subclassed)
        {
          clutter_gesture_set_state (gesture, CLUTTER_GESTURE_STATE_RECOGNIZING);
          if (clutter_gesture_get_state (gesture) != CLUTTER_GESTURE_STATE_RECOGNIZING)
            return;
        }
    }
  else
    {
      float distance =
        graphene_point_distance (&priv->press_coords, &coords, NULL, NULL);

      g_assert (priv->next_press_timeout_id > 0);
      g_clear_handle_id (&priv->next_press_timeout_id, g_source_remove);

      if (priv->is_touch != is_touch ||
          priv->press_button != press_button ||
          (priv->cancel_threshold >= 0 && distance > priv->cancel_threshold))
        {
          /* Instead of cancelling the gesture and throwing the point away, leave
           * it RECOGNIZING and treat the point like the first one. It would be
           * neat to cancel and then immediately recognize for the same point
           * but that's not possible due to ClutterGesture clearing points on
           * move to WAITING.
           */

          priv->n_presses_happened = 1;

          priv->is_touch = is_touch;
          priv->press_button = press_button;
          priv->modifier_state = modifier_state;
          priv->press_coords = coords;
        }
    }

  priv->next_press_timeout_id =
    g_timeout_add (get_next_press_timeout_ms (), next_press_timed_out, self);

  long_press_duration = priv->long_press_duration == 0
    ? get_default_long_press_duration()
    : priv->long_press_duration;

  g_assert (priv->long_press_timeout_id == 0);
  priv->long_press_timeout_id =
    g_timeout_add (long_press_duration, long_press_cb, self);

  set_pressed (self, TRUE);

  if (priv->is_subclassed)
    {
      if (CLUTTER_PRESS_GESTURE_GET_CLASS (self)->press)
        CLUTTER_PRESS_GESTURE_GET_CLASS (self)->press (self);
    }
  else
    {
      if (clutter_gesture_get_state (gesture) == CLUTTER_GESTURE_STATE_RECOGNIZING)
        g_signal_emit (self, obj_signals[PRESS], 0);
    }
}

static void
point_moved (ClutterGesture *gesture,
             unsigned int    sequence)
{
  ClutterPressGesture *self = CLUTTER_PRESS_GESTURE (gesture);
  ClutterPressGesturePrivate *priv =
    clutter_press_gesture_get_instance_private (self);
  graphene_point_t coords;

  clutter_gesture_get_point_coords_abs (gesture, sequence, &coords);

  float distance =
    graphene_point_distance (&coords, &priv->press_coords, NULL, NULL);

  if (priv->cancel_threshold >= 0 && distance > priv->cancel_threshold)
    clutter_gesture_set_state (gesture, CLUTTER_GESTURE_STATE_CANCELLED);
}

static void
point_ended (ClutterGesture *gesture,
             unsigned int    sequence)
{
  ClutterPressGesture *self = CLUTTER_PRESS_GESTURE (gesture);
  ClutterPressGesturePrivate *priv =
    clutter_press_gesture_get_instance_private (self);
  const ClutterEvent *event;
  ClutterModifierType modifier_state;

  g_clear_handle_id (&priv->long_press_timeout_id, g_source_remove);

  /* Exclude any button-mask so that we can compare
   * the press and release states properly
   */
  event = clutter_gesture_get_point_event (gesture, sequence);
  modifier_state = clutter_event_get_state (event) &
    ~(CLUTTER_BUTTON1_MASK | CLUTTER_BUTTON2_MASK | CLUTTER_BUTTON3_MASK |
      CLUTTER_BUTTON4_MASK | CLUTTER_BUTTON5_MASK);

  /* if press and release states don't match we
   * simply ignore modifier keys. i.e. modifier keys
   * are expected to be pressed throughout the whole
   * click
   */
  if (modifier_state != priv->modifier_state)
    priv->modifier_state = 0;

  if (priv->is_subclassed)
    {
      if (CLUTTER_PRESS_GESTURE_GET_CLASS (self)->release)
        CLUTTER_PRESS_GESTURE_GET_CLASS (self)->release (self);
    }
  else
    {
      if (clutter_gesture_get_state (gesture) == CLUTTER_GESTURE_STATE_RECOGNIZING)
        g_signal_emit (self, obj_signals[RELEASE], 0);

      clutter_gesture_set_state (gesture, CLUTTER_GESTURE_STATE_COMPLETED);
    }

  set_pressed (self, FALSE);

  /* If the next press has already timed out, we can cancel now. If it hasn't
   * timed out yet, we'll cancel on the timeout.
   */
  if (clutter_gesture_get_state (gesture) != CLUTTER_GESTURE_STATE_COMPLETED &&
      clutter_gesture_get_state (gesture) != CLUTTER_GESTURE_STATE_CANCELLED &&
      priv->next_press_timeout_id == 0)
    clutter_gesture_set_state (gesture, CLUTTER_GESTURE_STATE_CANCELLED);
}

static void
crossing_event (ClutterGesture    *gesture,
                unsigned int       point,
                ClutterEventType   type,
                uint32_t           time,
                ClutterEventFlags  flags,
                ClutterActor      *source_actor,
                ClutterActor      *related_actor)
{
  ClutterPressGesture *self = CLUTTER_PRESS_GESTURE (gesture);
  ClutterGestureState state = clutter_gesture_get_state (gesture);

  if ((state == CLUTTER_GESTURE_STATE_POSSIBLE || state == CLUTTER_GESTURE_STATE_RECOGNIZING) &&
      source_actor == clutter_actor_meta_get_actor (CLUTTER_ACTOR_META (self)))
    set_pressed (self, type == CLUTTER_ENTER);
}

static void
state_changed (ClutterGesture      *gesture,
               ClutterGestureState  old_state,
               ClutterGestureState  new_state)
{
  ClutterPressGesture *self = CLUTTER_PRESS_GESTURE (gesture);
  ClutterPressGesturePrivate *priv =
    clutter_press_gesture_get_instance_private (self);

  if (new_state == CLUTTER_GESTURE_STATE_COMPLETED ||
      new_state == CLUTTER_GESTURE_STATE_CANCELLED)
    {
      set_pressed (self, FALSE);

      g_clear_handle_id (&priv->next_press_timeout_id, g_source_remove);
      g_clear_handle_id (&priv->long_press_timeout_id, g_source_remove);

      priv->n_presses_happened = 0;
      priv->press_coords.x = 0;
      priv->press_coords.y = 0;
      priv->press_button = 0;
      priv->modifier_state = 0;
    }
}

static void
clutter_press_gesture_constructed (GObject *object)
{
  ClutterPressGesture *self = CLUTTER_PRESS_GESTURE (object);
  ClutterPressGesturePrivate *priv =
    clutter_press_gesture_get_instance_private (self);

  G_OBJECT_CLASS (clutter_press_gesture_parent_class)->constructed (object);

  priv->is_subclassed = CLUTTER_PRESS_GESTURE_GET_CLASS (self)->press != NULL ||
    CLUTTER_PRESS_GESTURE_GET_CLASS (self)->release != NULL ||
    CLUTTER_PRESS_GESTURE_GET_CLASS (self)->long_press != NULL;
}

static void
clutter_press_gesture_set_property (GObject      *gobject,
                                    unsigned int  prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  ClutterPressGesture *self = CLUTTER_PRESS_GESTURE (gobject);

  switch (prop_id)
    {
    case PROP_CANCEL_THRESHOLD:
      clutter_press_gesture_set_cancel_threshold (self, g_value_get_int (value));
      break;

    case PROP_LONG_PRESS_DURATION:
      clutter_press_gesture_set_long_press_duration (self, g_value_get_uint (value));
      break;

    case PROP_REQUIRED_BUTTON:
      clutter_press_gesture_set_required_button (self, g_value_get_uint (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
      break;
    }
}

static void
clutter_press_gesture_get_property (GObject      *gobject,
                                    unsigned int  prop_id,
                                    GValue       *value,
                                    GParamSpec   *pspec)
{
  ClutterPressGesture *self = CLUTTER_PRESS_GESTURE (gobject);

  switch (prop_id)
    {
    case PROP_CANCEL_THRESHOLD:
      g_value_set_int (value, clutter_press_gesture_get_cancel_threshold (self));
      break;

    case PROP_LONG_PRESS_DURATION:
      g_value_set_uint (value, clutter_press_gesture_get_long_press_duration (self));
      break;

    case PROP_PRESSED:
      g_value_set_boolean (value, clutter_press_gesture_get_pressed (self));
      break;

    case PROP_REQUIRED_BUTTON:
      g_value_set_uint (value, clutter_press_gesture_get_required_button (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
      break;
    }
}

static void
clutter_press_gesture_class_init (ClutterPressGestureClass *klass)
{
  ClutterGestureClass *gesture_class = CLUTTER_GESTURE_CLASS (klass);
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->constructed = clutter_press_gesture_constructed;
  gobject_class->set_property = clutter_press_gesture_set_property;
  gobject_class->get_property = clutter_press_gesture_get_property;

  gesture_class->should_handle_sequence = should_handle_sequence;
  gesture_class->point_began = point_began;
  gesture_class->point_moved = point_moved;
  gesture_class->point_ended = point_ended;
  gesture_class->crossing_event = crossing_event;
  gesture_class->state_changed = state_changed;

  /**
   * ClutterPressGesture:cancel-threshold:
   *
   * Threshold in pixels to cancel the gesture, use -1 to disable the threshold.
   */
  obj_props[PROP_CANCEL_THRESHOLD] =
    g_param_spec_int ("cancel-threshold",
                      "cancel-threshold",
                      "cancel-threshold",
                      -1, G_MAXINT, 0,
                      G_PARAM_READWRITE |
                      G_PARAM_STATIC_STRINGS |
                      G_PARAM_EXPLICIT_NOTIFY);

  /**
   * ClutterLongPressGesture:long-press-duration:
   *
   * The minimum duration of a press in milliseconds for it to be recognized
   * as a long press gesture.
   *
   * A value of 0 (default) will make the gesture use the value of the
   * #ClutterSettings:long-press-duration property.
   */
  obj_props[PROP_LONG_PRESS_DURATION] =
    g_param_spec_uint ("long-press-duration",
                       "long-press-duration",
                       "long-press-duration",
                       0, G_MAXUINT, 0,
                       G_PARAM_READWRITE |
                       G_PARAM_STATIC_STRINGS |
                       G_PARAM_EXPLICIT_NOTIFY);

  /**
   * ClutterPressGesture:pressed:
   *
   * Whether the clickable actor should be in "pressed" state
   */
  obj_props[PROP_PRESSED] =
    g_param_spec_boolean ("pressed",
                          "pressed",
                          "pressed",
                          FALSE,
                          G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS |
                          G_PARAM_EXPLICIT_NOTIFY);

  /**
   * ClutterPressGesture:required-button:
   *
   * The mouse button required for the press gesture to recognize.
   * Pass 0 to allow any button. Touch input is always handled as a press
   * of the primary button.
   */
  obj_props[PROP_REQUIRED_BUTTON] =
    g_param_spec_uint ("required-button",
                       "required-button",
                       "required-button",
                       0, G_MAXUINT, 0,
                       G_PARAM_READWRITE |
                       G_PARAM_STATIC_STRINGS |
                       G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_install_properties (gobject_class,
                                     PROP_LAST,
                                     obj_props);

  /**
   * ClutterPressGesture::press:
   * @gesture: the #ClutterPressGesture that emitted the signal
   *
   * The ::press signal is emitted when a button is pressed or a touch point
   * begins.
   */
  obj_signals[PRESS] =
    g_signal_new ("press",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 0, G_TYPE_NONE);

  /**
   * ClutterPressGesture::long-press:
   * @gesture: the #ClutterPressGesture that emitted the signal
   *
   * The ::long-press gesture is emitted on long press.
   */
  obj_signals[LONG_PRESS] =
    g_signal_new ("long-press",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 0, G_TYPE_NONE);

  /**
   * ClutterPressGesture::release:
   * @gesture: the #ClutterPressGesture that emitted the signal
   *
   * The ::release signal is emitted on button or touch release.
   */
  obj_signals[RELEASE] =
    g_signal_new ("release",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 0, G_TYPE_NONE);
}

static void
clutter_press_gesture_init (ClutterPressGesture *self)
{
  ClutterPressGesturePrivate *priv =
    clutter_press_gesture_get_instance_private (self);

  priv->pressed = FALSE;

  priv->cancel_threshold = get_default_cancel_threshold ();
  priv->long_press_duration = 0;

  priv->n_presses_happened = 0;
  priv->next_press_timeout_id = 0;
  priv->long_press_timeout_id = 0;

  priv->required_button = 0;

  priv->press_button = 0;
  priv->modifier_state = 0;
}

/**
 * clutter_press_gesture_new:
 *
 * Creates a new #ClutterPressGesture instance
 *
 * Returns: the newly created #ClutterPressGesture
 */
ClutterAction *
clutter_press_gesture_new (void)
{
  return g_object_new (CLUTTER_TYPE_PRESS_GESTURE, NULL);
}

/**
 * clutter_press_gesture_get_pressed:
 * @self: a #ClutterPressGesture
 *
 * Gets whether the press gesture actor should be in the "pressed" state.
 *
 * Returns: The "pressed" state
 */
gboolean
clutter_press_gesture_get_pressed (ClutterPressGesture *self)
{
  ClutterPressGesturePrivate *priv;

  g_return_val_if_fail (CLUTTER_IS_PRESS_GESTURE (self), FALSE);

  priv = clutter_press_gesture_get_instance_private (self);

  return priv->pressed;
}

/**
 * clutter_press_gesture_get_cancel_threshold:
 * @self: a #ClutterPressGesture
 *
 * Gets the movement threshold in pixels that cancels the press gesture.
 *
 * Returns: The cancel threshold in pixels
 */
int
clutter_press_gesture_get_cancel_threshold (ClutterPressGesture *self)
{
  ClutterPressGesturePrivate *priv;

  g_return_val_if_fail (CLUTTER_IS_PRESS_GESTURE (self), -1);

  priv = clutter_press_gesture_get_instance_private (self);

  return priv->cancel_threshold;
}

/**
 * clutter_press_gesture_set_cancel_threshold:
 * @self: a #ClutterPressGesture
 * @cancel_threshold: the threshold in pixels, or -1 to disable the threshold
 *
 * Sets the movement threshold in pixels that cancels the press gesture.
 *
 * See also #ClutterPressGesture:cancel-threshold.
 */
void
clutter_press_gesture_set_cancel_threshold (ClutterPressGesture *self,
                                            int                  cancel_threshold)
{
  ClutterPressGesturePrivate *priv;

  g_return_if_fail (CLUTTER_IS_PRESS_GESTURE (self));

  priv = clutter_press_gesture_get_instance_private (self);

  if (priv->cancel_threshold == cancel_threshold)
    return;

  priv->cancel_threshold = cancel_threshold;

  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_CANCEL_THRESHOLD]);
}

/**
 * clutter_press_gesture_get_long_press_duration:
 * @self: a #ClutterPressGesture
 *
 * Gets the minimum duration is milliseconds that's necessary for a long press
 * to recognize.
 */
unsigned int
clutter_press_gesture_get_long_press_duration (ClutterPressGesture *self)
{
  ClutterPressGesturePrivate *priv;

  g_return_val_if_fail (CLUTTER_IS_PRESS_GESTURE (self), 0);

  priv = clutter_press_gesture_get_instance_private (self);

  return priv->long_press_duration;
}

/**
 * clutter_press_gesture_set_long_press_duration:
 * @self: a #ClutterPressGesture
 * @long_press_duration: minimum duration is ms for long press to recognize
 *
 * Sets the minimum duration is milliseconds that's necessary for a long press
 * to recognize.
 *
 * Pass -1 to use the default from #ClutterSettings:long-press-duration.
 */
void
clutter_press_gesture_set_long_press_duration (ClutterPressGesture *self,
                                               unsigned int         long_press_duration)
{
  ClutterPressGesturePrivate *priv;

  g_return_if_fail (CLUTTER_IS_PRESS_GESTURE (self));

  priv = clutter_press_gesture_get_instance_private (self);

  if (priv->long_press_duration == long_press_duration)
    return;

  priv->long_press_duration = long_press_duration;

  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_LONG_PRESS_DURATION]);
}

/**
 * clutter_press_gesture_triggers_context_menu:
 * @self: a #ClutterPressGesture
 *
 * Retrieves whether the press should trigger a context menu, usually this
 * means that the secondary mouse button has been pressed.
 *
 * Returns: %TRUE if the press should trigger a context menu
 */
gboolean
clutter_press_gesture_triggers_context_menu (ClutterPressGesture *self)
{
  ClutterPressGesturePrivate *priv;

  g_return_val_if_fail (CLUTTER_IS_PRESS_GESTURE (self), FALSE);

  priv = clutter_press_gesture_get_instance_private (self);

  return priv->press_button == CLUTTER_BUTTON_SECONDARY;
}

/**
 * clutter_press_gesture_get_button:
 * @self: a #ClutterPressGesture
 *
 * Retrieves the button that was pressed.
 *
 * Returns: the button value
 */
unsigned int
clutter_press_gesture_get_button (ClutterPressGesture *self)
{
  ClutterPressGesturePrivate *priv;

  g_return_val_if_fail (CLUTTER_IS_PRESS_GESTURE (self), 0);

  priv = clutter_press_gesture_get_instance_private (self);

  return priv->press_button;
}

/**
 * clutter_press_gesture_get_state:
 * @self: a #ClutterPressGesture
 *
 * Retrieves the modifier state of the press gesture.
 *
 * Returns: the modifier state parameter, or 0
 */
ClutterModifierType
clutter_press_gesture_get_state (ClutterPressGesture *self)
{
  ClutterPressGesturePrivate *priv;

  g_return_val_if_fail (CLUTTER_IS_PRESS_GESTURE (self), 0);

  priv = clutter_press_gesture_get_instance_private (self);

  return priv->modifier_state;
}

/**
 * clutter_press_gesture_get_coords:
 * @self: a #ClutterPressGesture
 * @coords_out: (out): a #graphene_point_t
 *
 * Retrieves the coordinates of the press.
 */
void
clutter_press_gesture_get_coords (ClutterPressGesture *self,
                                  graphene_point_t    *coords_out)
{
  ClutterPressGesturePrivate *priv;
  float x, y;
  ClutterActor *action_actor;

  g_return_if_fail (CLUTTER_IS_PRESS_GESTURE (self));
  g_return_if_fail (coords_out != NULL);

  priv = clutter_press_gesture_get_instance_private (self);

  x = priv->press_coords.x;
  y = priv->press_coords.y;

  action_actor = clutter_actor_meta_get_actor (CLUTTER_ACTOR_META (self));
  if (action_actor)
    clutter_actor_transform_stage_point (action_actor, x, y, &x, &y);

  coords_out->x = x;
  coords_out->y = y;
}

/**
 * clutter_press_gesture_get_coords_abs:
 * @self: a #ClutterPressGesture
 * @coords_out: (out): a #graphene_point_t
 *
 * Retrieves the coordinates of the press in absolute coordinates.
 */
void
clutter_press_gesture_get_coords_abs (ClutterPressGesture *self,
                                      graphene_point_t    *coords_out)
{
  ClutterPressGesturePrivate *priv;

  g_return_if_fail (CLUTTER_IS_PRESS_GESTURE (self));
  g_return_if_fail (coords_out != NULL);

  priv = clutter_press_gesture_get_instance_private (self);

  coords_out->x = priv->press_coords.x;
  coords_out->y = priv->press_coords.y;
}

/**
 * clutter_press_gesture_get_n_presses:
 * @self: a #ClutterPressGesture
 *
 * Retrieves the number of presses that happened on the gesture.
 *
 * Returns: The number of presses
 */
unsigned int
clutter_press_gesture_get_n_presses (ClutterPressGesture *self)
{
  ClutterPressGesturePrivate *priv;

  g_return_val_if_fail (CLUTTER_IS_PRESS_GESTURE (self), 0);

  priv = clutter_press_gesture_get_instance_private (self);

  return priv->n_presses_happened;
}

/**
 * clutter_press_gesture_get_required_button:
 * @self: a #ClutterPressGesture
 *
 * Gets the mouse button required for the press gesture to recognize.
 *
 * Returns: The mouse button required to recognize
 */
unsigned int
clutter_press_gesture_get_required_button (ClutterPressGesture *self)
{
  ClutterPressGesturePrivate *priv;

  g_return_val_if_fail (CLUTTER_IS_PRESS_GESTURE (self), 0);

  priv = clutter_press_gesture_get_instance_private (self);

  return priv->required_button;
}

/**
 * clutter_press_gesture_set_required_button:
 * @self: a #ClutterPressGesture
 * @required_button: mouse button required for the gesture to recognize
 *
 * Sets the mouse button required for the press gesture to recognize.
 * Pass 0 to allow any button. Touch input is always handled as a press
 * of the primary button.
 */
void
clutter_press_gesture_set_required_button (ClutterPressGesture *self,
                                           unsigned int         required_button)
{
  ClutterPressGesturePrivate *priv;

  g_return_if_fail (CLUTTER_IS_PRESS_GESTURE (self));

  priv = clutter_press_gesture_get_instance_private (self);

  if (priv->required_button == required_button)
    return;

  priv->required_button = required_button;

  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_REQUIRED_BUTTON]);
}
